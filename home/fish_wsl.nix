{ ... }:

{
  home.file = {
    ".config/fish/config.fish".source = ./dotfiles/wsl.fish;
  };
}
