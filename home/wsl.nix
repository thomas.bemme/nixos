{ ... }:

{
  imports =
    [
      ./base.nix
      ./fish_wsl.nix
      ./dconf.nix
    ];
}
