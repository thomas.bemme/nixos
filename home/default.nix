{ ... }:

{
  imports =
    [
      ./base.nix
      ./fish.nix
    ];
}
