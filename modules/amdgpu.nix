{ pkgs, inputs, vars, ... }:

{
  hardware.graphics.extraPackages = with pkgs; [
    rocmPackages.clr.icd
  ];

  users.users.${vars.user} = {
    packages = with pkgs; [
      amdgpu_top
      blender-hip
    ];
  };

  environment.systemPackages = with pkgs; [
    corectrl
    vulkan-tools
  ];

  # Corectrl without password
  security.polkit = {
    extraConfig = ''
      polkit.addRule(function(action, subject) {
          if ((action.id == "org.corectrl.helper.init" ||
               action.id == "org.corectrl.helperkiller.init") &&
              subject.local == true &&
              subject.active == true &&
              subject.isInGroup("wheel")) {
                  return polkit.Result.YES;
          }
      });
    '';
  };
}
